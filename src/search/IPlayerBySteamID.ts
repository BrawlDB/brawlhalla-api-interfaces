export interface IPlayerBySteamID {
  brawlhallaID: number;
  name: string;
}
