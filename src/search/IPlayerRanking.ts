import { ILegend } from '../ILegend';
import { TValidDivision, TValidRegions, TValidTier } from '../types';

export interface IPlayerRanking {
  brawlhallaID: number;
  name: string;
  rank: number;
  bestLegend: ILegend;
  bestLegendGames: number;
  bestLegendWins: number;
  rating: number;
  peakRating: number;
  tier: TValidTier;
  division: TValidDivision;
  games: number;
  wins: number;
  region: TValidRegions;
}
