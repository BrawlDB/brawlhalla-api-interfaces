import { IWeapon } from '../IWeapon';

export interface IWeaponStats {

  weapon: IWeapon;
  time: number;
  games: number;
  wins: number;
  kos: number;

}
