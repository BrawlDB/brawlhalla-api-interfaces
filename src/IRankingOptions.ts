import { TValidBrackets, TValidRegions } from './types';

export interface IRankingOptions {
  bracket?: TValidBrackets;
  region?: TValidRegions;
  /**
   * The player name which is currently being searched
   * Note: The search is based on the first characters of the player name
   * Therefore, searching for a middle part of the name will not work.
   */
  name?: string;

  /**
   * The page number
   */
  page: number;
}
