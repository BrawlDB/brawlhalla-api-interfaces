import { TClanRank } from '../types';

export interface IClanMember {
  brawlhallaID: number;
  name: string;
  rank: TClanRank;
  contributedExperience: number;
  joinDate: number;
}
