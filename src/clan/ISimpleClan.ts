// IClan information contained when searching a user's stats
export interface ISimpleClan {
  name: string;
  id: number;
  xp: number;
  personalXp: number;
}
