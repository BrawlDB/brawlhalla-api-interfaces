export type TValidRegions = 'all' | 'us-e' | 'us-w' | 'eu' | 'sea' | 'brz' | 'aus';
export type TValidBrackets = '1v1' | '2v2';
export type TValidTier = 'Tin' | 'Bronze' | 'Silver' | 'Gold' | 'Platinum' | 'Diamond';

// 0 to 5 are the division possibilities within a tier.
/* tslint:disable-next-line:no-magic-numbers */
export type TValidDivision = 0 | 1 | 2 | 3 | 4 | 5;

export type TClanRank = 'Leader' | 'Officer' | 'Member' | 'Recruit';

export type TWeaponName =
  | 'Unarmed'
  | 'Axe'
  | 'Hammer'
  | 'Sword'
  | 'Blasters'
  | 'Rocket Lance'
  | 'Spear'
  | 'Katar'
  | 'Axe'
  | 'Bow'
  | 'Gauntlets'
  | 'Scythe'
  | 'Cannon'
  | 'Orb'
  | string;

export type TLegendName =
  | 'Bödvar'
  | 'Cassidy'
  | 'Orion'
  | 'Lord Vraxx'
  | 'Gnash'
  | 'Queen Nai'
  | 'Lucien'
  | 'Hattori'
  | 'Sir Roland'
  | 'Scarlet'
  | 'Thatch'
  | 'Ada'
  | 'Sentinel'
  | 'Teros'
  | 'Ember'
  | 'Brynn'
  | 'Asuri'
  | 'Barraza'
  | 'Ulgrim'
  | 'Azoth'
  | 'Koji'
  | 'Diana'
  | 'Jhala'
  | 'Kor'
  | 'Wu Shang'
  | 'Val'
  | 'Ragnir'
  | 'Cross'
  | 'Mirage'
  | 'Nix'
  | 'Mordex'
  | 'Yumiko'
  | 'Artemis'
  | 'Caspian'
  | 'Sidra'
  | 'Xull'
  | 'Isaiah'
  | 'Kaya'
  | 'Jiro'
  | 'Lin Fei'
  | 'Zariel'
  | 'Rayman'
  | 'Dusk'
  | string
  ;
